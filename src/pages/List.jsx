import React, {useState} from 'react';
import { Alert } from 'react-bootstrap';
import MyForm from '../components/MyForm'
import Todo from '../components/Todo';


export function List () {
    
    const [todos, setTodos] = useState([]);

    const addTodo = text => {
        const newTodos = [...todos, { text , isCompleted:false}];
        setTodos(newTodos);
      };

      const completeTodo = index => {
        const newTodos = [...todos];
        newTodos[index].isCompleted = true ;
        setTodos(newTodos);
      };

      const removeTodo = index => {
          const newTodos = [...todos];
          newTodos.splice(index, 1);
          setTodos(newTodos);

      }



  


     


    return(
        <React.Fragment>
            
            <h2 className="p-5">Todo App in ReactJS</h2>

            <div className="todo-list">
                {todos.length> 0 ? todos.map((todo, index) => (
                 <Todo

                    key={index}
                    number={index+1}
                    index={index}
                    todo={todo}
                    completeTodo={completeTodo}
                    removeTodo={removeTodo}
                    
                 />
                 )) : <Alert className="mt-5" variant='danger' >There are no ToDos</Alert> }
                <MyForm addTodo={addTodo} />

            </div>
  

      
         
        </React.Fragment>
    )
     
        
}