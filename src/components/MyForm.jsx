import React, { useState } from 'react';
import { Button,  Form, FormControl, InputGroup } from 'react-bootstrap';


export default function MyForm  ({addTodo})  {
    const [value, setValue] = useState("")

    const handleSubmit = e => {
        e.preventDefault();
        if(!value) return
        addTodo(value);
        setValue('')
    }

return (
    <Form onSubmit={handleSubmit} className="p-5" inline >
        <div className="container">

            <InputGroup className="mb-3">
                <FormControl
                value={value}
                onChange={e => setValue(e.target.value)}
                id="input"
                placeholder="Freaking Todo"
                />

                <InputGroup.Append>
                    <Button type="submit" variant="success">+ Add Todo</Button>
                </InputGroup.Append>
            </InputGroup>

        </div>
    </Form>
)
   
}