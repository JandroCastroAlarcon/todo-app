import React from 'react'

export default function Todo ({todo,number, index, completeTodo, removeTodo}) {

  

    return(
        <div className="todo container row mt-5">
            <div className="col-1">
                {number}
            </div>
            <div className="col-1">
                <input onClick={() => completeTodo(index)} type="checkbox"/>
            </div>
            <div className="col " style={{ textDecoration: todo.isCompleted ? "line-through" : ""  }}>
                {todo.text}
            </div>
            <div>
                <button className="col " onClick={() => removeTodo(index)} className="btn btn-danger">Delete</button>
            </div>


        </div>


    )
}